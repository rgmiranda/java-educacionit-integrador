-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.34-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para autos
CREATE DATABASE IF NOT EXISTS `autos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `autos`;

-- Volcando estructura para tabla autos.autos
CREATE TABLE IF NOT EXISTS `autos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` float NOT NULL,
  `alto` smallint(6) NOT NULL DEFAULT '0',
  `ancho` smallint(6) NOT NULL DEFAULT '0',
  `largo` smallint(6) NOT NULL DEFAULT '0',
  `id_color` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  `id_modelo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `colores_autos_fk` (`id_color`),
  KEY `marcas_autos_fk` (`id_marca`),
  KEY `modelos_autos_fk` (`id_modelo`),
  CONSTRAINT `colores_autos_fk` FOREIGN KEY (`id_color`) REFERENCES `colores` (`id`),
  CONSTRAINT `marcas_autos_fk` FOREIGN KEY (`id_marca`) REFERENCES `marcas_autos` (`id`),
  CONSTRAINT `modelos_autos_fk` FOREIGN KEY (`id_modelo`) REFERENCES `modelos_autos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.categorias_vendedores
CREATE TABLE IF NOT EXISTS `categorias_vendedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `comision` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `documento` int(11) NOT NULL,
  `domicilio` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.colores
CREATE TABLE IF NOT EXISTS `colores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.marcas_autos
CREATE TABLE IF NOT EXISTS `marcas_autos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.modelos_autos
CREATE TABLE IF NOT EXISTS `modelos_autos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `id_marca` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modelos_marcas_autos_fk` (`id_marca`),
  CONSTRAINT `modelos_marcas_autos_fk` FOREIGN KEY (`id_marca`) REFERENCES `marcas_autos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.vendedores
CREATE TABLE IF NOT EXISTS `vendedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `documento` int(11) NOT NULL,
  `domicilio` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cuil` varchar(255) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categorias_vendedores_fk` (`id_categoria`),
  CONSTRAINT `categorias_vendedores_fk` FOREIGN KEY (`id_categoria`) REFERENCES `categorias_vendedores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla autos.ventas
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio_final_auto` float NOT NULL,
  `impuestos` float NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `cantidad_cuotas` smallint(6) NOT NULL DEFAULT '1',
  `monto_cuota` float NOT NULL,
  `id_auto` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ventas_autos_fk` (`id_auto`),
  KEY `ventas_cliente_fk` (`id_cliente`),
  KEY `ventas_vendedores_fk` (`id_vendedor`),
  CONSTRAINT `ventas_autos_fk` FOREIGN KEY (`id_auto`) REFERENCES `autos` (`id`),
  CONSTRAINT `ventas_cliente_fk` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `ventas_vendedores_fk` FOREIGN KEY (`id_vendedor`) REFERENCES `vendedores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
