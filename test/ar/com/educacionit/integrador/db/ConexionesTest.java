/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public class ConexionesTest {
    
    public ConexionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConectar() {
        Connection cnx = null;
        try {
            InputStream input = this.getClass().getResourceAsStream("/ar/com/educacionit/integrador/config/db/autos.properties");
            Properties props = new Properties();
            props.load(input);
            cnx = Conexiones.conectar(props);
            assertNotNull(cnx);
        } catch (IOException | SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ConexionesTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ConexionesTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
