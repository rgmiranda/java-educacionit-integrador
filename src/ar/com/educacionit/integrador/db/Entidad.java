/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador.db;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public abstract class Entidad {
    
    /**
     * Recupera el nombre de la tabla sobre la que trabaja la entidad
     * @return String
     */
    protected abstract String getNombreTabla();
    
    
}
