/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public class Conexiones {
    private static final HashMap<Integer, Connection> conexiones = new HashMap<>();
    
    public static Connection conectar(Properties config) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        Integer key = config.hashCode();
        
        if (!conexiones.containsKey(key)) {
            String dbname = config.getProperty("dbname");
            String host = config.getProperty("host");
            String driver = config.getProperty("driver");
            String driverClass = config.getProperty("class");
            String user = config.getProperty("user");
            String passwd = config.getProperty("passwd");
            String cadenaConexion = String.format("jdbc:%s://%s/%s?user=%s&password=%s&serverTimezone=UTC", driver, host, dbname, user, passwd);
            Driver jdbcDriver = (Driver) Class.forName(driverClass).newInstance();
            Connection cnn = jdbcDriver.connect(cadenaConexion, null);
            conexiones.put(key, cnn);
        }
        return conexiones.get(key);
    }
}
