/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador.dominio.autos;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public class Modelo {
    private int id;
    private String descripcion;
    private Marca marca;

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
