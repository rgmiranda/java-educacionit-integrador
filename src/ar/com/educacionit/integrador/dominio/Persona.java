/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador.dominio;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public abstract class Persona {
    private int id;
    private String nombre;
    private String apellido;
    private int documento;
    private String domicilio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    
    public Persona() {
        this(null, null, 0, null);
    }
    
    public Persona(String nombre, String apellido) {
        this(nombre, apellido, 0, null);
    }
    
    public Persona(String nombre, String apellido, int documento) {
        this(nombre, apellido, documento, null);
    }
    
    public Persona(String nombre, String apellido, int documento, String domicilio) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.documento = documento;
        this.domicilio = domicilio;
    }
    
    @Override
    public String toString() {
        return this.nombre + " " + this.apellido;
    }
}
