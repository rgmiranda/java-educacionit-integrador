/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.integrador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Ricardo Miranda <rmiranda@jussantiago.gov.ar>
 */
public class Principal extends Application {
    
    private Stage primaryStage;
    private static Principal instance;
    
    public static Principal getInstance() {
        return Principal.instance;
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ventanas/VentanaPrincipal.fxml"));
        Principal.instance = this;
        
        Scene scene = new Scene(root);
        this.primaryStage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void mostrarVentana(String fxml, String title, Stage ventanaSuperior) {
        try {
            if (ventanaSuperior == null) {
                ventanaSuperior = primaryStage;
            }
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(fxml));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.initModality(Modality.NONE);
            stage.initOwner(ventanaSuperior);
            stage.setTitle(title);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Alert error = new Alert(Alert.AlertType.ERROR, "Error", ButtonType.CLOSE);
            error.setContentText(ex.getMessage());
            error.setHeaderText("Se ha producido un error al abrir una ventana");
            error.show();
        }
    }
    
}
